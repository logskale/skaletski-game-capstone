using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularMovement : MonoBehaviour
{
    [SerializeField] float initialRadius = 2f;
    [SerializeField] float expansionSpeed = 0.5f;
    [SerializeField] float rotationSpeed = 60f;

    private float currentRadius;

    void Start()
    {
        currentRadius = initialRadius;
    }

    void Update()
    {
        // Increase the radius over time
        currentRadius += expansionSpeed * Time.deltaTime;

        // Calculate the position on the circle based on time
        float angle = Time.time * rotationSpeed * Mathf.Deg2Rad;
        float x = Mathf.Cos(angle) * currentRadius;
        float z = Mathf.Sin(angle) * currentRadius;

        // Set the position of the object
        transform.position = transform.parent.position + new Vector3(x, 0f, z);
    }
}
