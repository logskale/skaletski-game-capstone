using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TombTriggerRight : MonoBehaviour
{
    [SerializeField] private Transform spawnPoint; // Point to spawn the object
    [SerializeField] private GameObject objectToSpawn; // Object to spawn
    [SerializeField] private Renderer objectRenderer; // Renderer of the object to change material
    [SerializeField] private Material newMaterial; // New material to apply
    private Rigidbody spawnedObjectRb; // Rigidbody of the spawned object
    private bool onCooldown = false; // Flag to indicate if the trigger is on cooldown

    private void OnTriggerEnter(Collider other)
    {
        if (!onCooldown && other.CompareTag("Player")) // Check if not on cooldown and the entering object is the player
        {
            SpawnObject();
            ApplyForce();
            StartCoroutine(CooldownCoroutine());
            StartCoroutine(MaterialChangeCoroutine());
        }
    }

    private void SpawnObject()
    {
        if (spawnPoint != null && objectToSpawn != null)
        {
            GameObject newObject = Instantiate(objectToSpawn, spawnPoint.position, spawnPoint.rotation);
            spawnedObjectRb = newObject.GetComponent<Rigidbody>();
        }
        else
        {
            Debug.LogError("Spawn point or object to spawn is not assigned!");
        }
    }

    private void ApplyForce()
    {
        if (spawnedObjectRb != null)
        {
            spawnedObjectRb.AddForce(Vector3.right * 20f, ForceMode.Impulse); // Apply force in negative Z direction
        }
        else
        {
            Debug.LogError("Spawned object Rigidbody is not assigned!");
        }
    }

    private IEnumerator CooldownCoroutine()
    {
        onCooldown = true;
        yield return new WaitForSeconds(7f); // Cooldown duration
        onCooldown = false;
    }

    private IEnumerator MaterialChangeCoroutine()
    {
        if (objectRenderer != null && newMaterial != null)
        {
            Material originalMaterial = objectRenderer.material;
            objectRenderer.material = newMaterial;
            yield return new WaitForSeconds(2f); // Duration of material change
            objectRenderer.material = originalMaterial; // Revert to original material
        }
        else
        {
            Debug.LogError("Object renderer or new material is not assigned!");
        }
    }
}

