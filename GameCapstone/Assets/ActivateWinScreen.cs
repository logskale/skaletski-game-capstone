using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateWinScreen : MonoBehaviour
{
    [SerializeField] Canvas WinnerCanvas;

    public void ActivateCanvas()
    {
        WinnerCanvas.enabled = true;
    }
    public void DeactivateCanvas()
    {
        WinnerCanvas.enabled = false;
    }
}
