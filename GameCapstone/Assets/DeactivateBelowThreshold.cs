using UnityEngine;

public class DeactivateBelowThreshold : MonoBehaviour
{
    [SerializeField] private float threshold = -50f; // Threshold for y-position
    private PlayerTagCheck ptc;

    void Start()
    {
        // Find the PlayerTagCheck script in the scene
        ptc = FindObjectOfType<PlayerTagCheck>();
        if (ptc == null)
        {
            Debug.LogError("PlayerTagCheck not found in the scene!");
        }
    }

    void Update()
    {
        // Check if the y-position of the GameObject falls below the threshold
        if (transform.position.y < threshold)
        {
            // Deactivate the GameObject
            gameObject.SetActive(false);
            if (ptc != null)
                ptc.activePlayerCount--;
        }
    
    }
}
