using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerFourScore : MonoBehaviour
{
    [SerializeField] PlayerTagCheck getPlayer;
    public TextMeshProUGUI textMeshPro;
    private bool isActive;

    // Start is called before the first frame update
    void Start()
    {
        if (getPlayer.totalPlayers > 3)
        {
            if (getPlayer.players[3] != null)
            {
                if (textMeshPro == null)
                    textMeshPro = GetComponent<TextMeshProUGUI>();
                isActive = true;
            }
            else
            {
                isActive = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            textMeshPro.text = "Player 4: " + getPlayer.players[3].GetComponent<PlayerScore>().GetScore();
        }
    }
}
