using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;
using UnityEngine.InputSystem;


public class setPlayerColor : MonoBehaviour
{
    private PlayerTagCheck numPlayers;
    private PlayerConfigurationManager colorArray;


    // Start is called before the first frame update
    void Start()
    {
        numPlayers = FindObjectOfType<PlayerTagCheck>();
        colorArray = FindObjectOfType<PlayerConfigurationManager>();

        if (colorArray == null)
        {
            Debug.LogError("PlayerConfigurationManager not found in the scene!");
            return; // Exit the function to prevent further execution
        }

        if (numPlayers.totalPlayers == 2)
        {
            Debug.Log(numPlayers.totalPlayers);

            // Find all child objects with the name "Player1" and "Player2"
            GameObject player1 = GameObject.Find("Player1");
            GameObject player2 = GameObject.Find("Player2");

            SetPlayerColors(player1, 2);
            SetPlayerColors(player2, 3);
        }
        else if (numPlayers.totalPlayers == 3)
        {
            Debug.Log(numPlayers.totalPlayers);

            // Find all child objects with the name "Player1" and "Player2"
            GameObject player1 = GameObject.Find("Player1");
            GameObject player2 = GameObject.Find("Player2");
            GameObject player3 = GameObject.Find("Player3");

            SetPlayerColors(player1, 2);
            SetPlayerColors(player2, 3);
            SetPlayerColors(player3, 4);
        }
    }

    void SetPlayerColors(GameObject player, int splitScreenIndex)
    {
        if (player == null)
        {
            Debug.LogError("Player not found!");
            return;
        }

        // Find all child objects with the name "MushroomOneRed_WithDodge", etc.
        GameObject mushroomOneRed = player.transform.Find("MushroomOneRed_WithDodge").gameObject;
        GameObject mushroomOneGreen = player.transform.Find("MushroomOneGreen_WithDodge").gameObject;
        GameObject mushroomOneYellow = player.transform.Find("MushroomOneYellow_WithDodge").gameObject;
        GameObject mushroomOneBlue = player.transform.Find("MushroomOneBlue_WithDodge2").gameObject;

        if (splitScreenIndex == 2)
        {
            Debug.Log("trying to set player 1 color");
            // Use colorArray.newColor list at index 0
            string playerColor = colorArray.newColorList[0];

            // Destroy objects based on color
            if (playerColor != "Red (UnityEngine.Material)")
                Destroy(mushroomOneRed);
            if (playerColor != "Green (UnityEngine.Material)")
                Destroy(mushroomOneGreen);
            if (playerColor != "Yellow (UnityEngine.Material)")
                Destroy(mushroomOneYellow);
            if (playerColor != "Blue (UnityEngine.Material)")
                Destroy(mushroomOneBlue);
        }
        else if (splitScreenIndex == 3)
        {
            Debug.Log("trying to set player 2 color");
            // Use colorArray.newColor list at index 1
            string playerColor = colorArray.newColorList[1];

            // Destroy objects based on color
            if (playerColor != "Red (UnityEngine.Material)")
                Destroy(mushroomOneRed);
            if (playerColor != "Green (UnityEngine.Material)")
                Destroy(mushroomOneGreen);
            if (playerColor != "Yellow (UnityEngine.Material)")
                Destroy(mushroomOneYellow);
            if (playerColor != "Blue (UnityEngine.Material)")
                Destroy(mushroomOneBlue);
        }
        else if (splitScreenIndex == 4)
        {
            Debug.Log("trying to set player 2 color");
            // Use colorArray.newColor list at index 1
            string playerColor = colorArray.newColorList[2];

            // Destroy objects based on color
            if (playerColor != "Red (UnityEngine.Material)")
                Destroy(mushroomOneRed);
            if (playerColor != "Green (UnityEngine.Material)")
                Destroy(mushroomOneGreen);
            if (playerColor != "Yellow (UnityEngine.Material)")
                Destroy(mushroomOneYellow);
            if (playerColor != "Blue (UnityEngine.Material)")
                Destroy(mushroomOneBlue);
        }
        else if (splitScreenIndex == 5)
        {
            Debug.Log("trying to set player 2 color");
            // Use colorArray.newColor list at index 1
            string playerColor = colorArray.newColorList[3];

            // Destroy objects based on color
            if (playerColor != "Red (UnityEngine.Material)")
                Destroy(mushroomOneRed);
            if (playerColor != "Green (UnityEngine.Material)")
                Destroy(mushroomOneGreen);
            if (playerColor != "Yellow (UnityEngine.Material)")
                Destroy(mushroomOneYellow);
            if (playerColor != "Blue (UnityEngine.Material)")
                Destroy(mushroomOneBlue);
        }
        else if (splitScreenIndex == 6)
        {
            Debug.Log("trying to set player 2 color");
            // Use colorArray.newColor list at index 1
            string playerColor = colorArray.newColorList[4];

            // Destroy objects based on color
            if (playerColor != "Red (UnityEngine.Material)")
                Destroy(mushroomOneRed);
            if (playerColor != "Green (UnityEngine.Material)")
                Destroy(mushroomOneGreen);
            if (playerColor != "Yellow (UnityEngine.Material)")
                Destroy(mushroomOneYellow);
            if (playerColor != "Blue (UnityEngine.Material)")
                Destroy(mushroomOneBlue);
        }
        GameObject player1 = GameObject.Find("Player1");
        GameObject player2 = GameObject.Find("Player2");
        GameObject player3 = GameObject.Find("Player3");
    }

    void DeactivateAndReactivateAnimator(GameObject player)
    {
        if (player != null)
        {
            Animator animator = player.GetComponent<Animator>();
            if (animator != null)
            {
                // Deactivate the animator
                animator.enabled = false;
                // Reactivate the animator
                animator.enabled = true;
            }
        }
    }
}
