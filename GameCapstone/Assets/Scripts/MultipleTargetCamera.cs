using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Camera))]
public class MultipleTargetCamera : MonoBehaviour
{
    public List<Transform> targets;

    public Vector3 offset;

    private Vector3 velocity;
    private Camera cam;

    public float smoothTime = 5f;

    public float minZoom = 40f;
    public float maxZoom = 10;
    [SerializeField]public float zoomLimiter = 10f;
    [SerializeField] private float zBuffer = -5f;

    [SerializeField] PlayerTagCheck PTC;

    private void Start()
    {
        cam = GetComponent<Camera>();
        targets = new List<Transform>(); // Initialize targets list
    }

    private void LateUpdate()
    {
        RemakeTargetsList(); // Remake targets list
        if (targets.Count == 0)
            return;
        Move();
        Zoom();

    }

    void Zoom()
    {
        float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance()/zoomLimiter);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, newZoom, Time.deltaTime);
    }

    void Move()
    {
        Vector3 centerPoint = GetCenterPoint();
        Vector3 newPosition = centerPoint + offset;

        // Set y position to the current y position of the camera
        newPosition.y = transform.position.y;

        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
    }

    Vector3 GetCenterPoint()
    {
        if (targets.Count == 1)
        {
            return targets[0].position;
        }
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i=0; i<targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        Vector3 centerPoint = bounds.center;
        return centerPoint;
    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        return bounds.size.x;
    }

    void RemakeTargetsList()
    {
        targets.Clear(); // Clear existing targets

        // Find all active GameObjects with the tag "Player" and add their transforms to the targets list
        GameObject[] playerObjects = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject playerObject in playerObjects)
        {
            if (playerObject.activeInHierarchy)
            {
                targets.Add(playerObject.transform);
            }
        }
    }
}
