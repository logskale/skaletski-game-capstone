using UnityEngine;
using UnityEngine.UI;

public class ShowHideTextBox : MonoBehaviour
{
    public GameObject textBox; // Reference to the text box

    // This method is called when the button is clicked
    public void ToggleTextBox()
    {
        // Check if the text box is active
        if (textBox.activeSelf)
        {
            textBox.SetActive(false); // If active, hide the text box
        }
        else
        {
            textBox.SetActive(true); // If inactive, show the text box
        }
    }
}