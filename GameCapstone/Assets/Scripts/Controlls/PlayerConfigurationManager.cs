using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerConfigurationManager : MonoBehaviour
{
    public List<PlayerConfiguration> playerConfigs;

    [SerializeField]
    private int MaxPlayers = 4;
    private int MinPlayers = 2;
    public string colorChosen;
    public List<string> colorList;
    public string[] newColorList;

    [SerializeField] private GameObject tutorialCanvas;

    public static PlayerConfigurationManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Debug.Log("SINGLETON- Tryign to create another instance of singleton!");
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(Instance);
            playerConfigs = new List<PlayerConfiguration>();
            colorList = new List<string>();
            newColorList = new string[4];
        }
    }

    public void SetPlayerColor(int index, Material color)
    {
        playerConfigs[index].PlayerMaterial = color;

        Debug.Log("Player color set "+ color);
        newColorList[playerConfigs[index].PlayerIndex] = color.ToString();

        colorChosen = color.ToString();
    }

    public void ReadyPlayer(int index)
    {
        playerConfigs[index].IsReady = true;
        if (playerConfigs.Count >= MinPlayers && playerConfigs.All(p=>p.IsReady==true))
        {
            //put up an infographic for movement purposes
            tutorialCanvas.SetActive(true);
            Time.timeScale = 0; // Pause the game
            // Start a coroutine to resume the game after the specified duration
            StartCoroutine(ResumeAfterDelay(10));
            
        }
    }

    public void HandlePlayerJoin(PlayerInput pi)
    {
        if (playerConfigs.Count < MaxPlayers)
        {
            Debug.Log("Player Joined" + pi.playerIndex);
            pi.transform.SetParent(transform);
            if (!playerConfigs.Any(p => p.PlayerIndex == pi.playerIndex))
            {
                playerConfigs.Add(new PlayerConfiguration(pi));
            }
        }
        
    }
    public List<PlayerConfiguration> GetPlayerConfigs()
    {
        return playerConfigs;
    }
    private IEnumerator ResumeAfterDelay(float duration)
    {
        yield return new WaitForSecondsRealtime(duration); // Wait for the specified duration
        Time.timeScale = 1; // Resume the game
        tutorialCanvas.SetActive(false);
        //Load the first level
        SceneManager.LoadScene("Level1");
    }
}

public class PlayerConfiguration
{
    public PlayerConfiguration(PlayerInput pi)
    {
        PlayerIndex = pi.playerIndex;
        Input = pi;
    }


    public PlayerInput Input { get; private set; }

    public int PlayerIndex { get; private set; }

    public bool IsReady { get; set; }

    public Material PlayerMaterial { get; set; }
}
