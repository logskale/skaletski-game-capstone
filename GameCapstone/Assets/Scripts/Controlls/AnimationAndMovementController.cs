using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;


public class AnimationAndMovementController : MonoBehaviour
{
    PlayerInputActions playerInput;
    CharacterController characterController;
    Animator animator;

    int isWalkingHash;
    int isRunningHash;
    int DodgeHash;

    Vector2 currentMovementInput;
    Vector3 currentMovement;
    Vector3 currentRunMovement;
    bool isMovementPressed;
    bool isRunPressed;
    [SerializeField] float rotationFactorPerFrame = 15.0f;
    float runMultiplier = .2f;
    [SerializeField] float magicSpeed= 500;

    [Header("Magic")]
    [SerializeField] private GameObject firePrefab;
    [SerializeField] private GameObject earthPrefab;
    [SerializeField] private GameObject icePrefab;
    [SerializeField] private Transform firePosition;
    [SerializeField] private float magicCooldown=1f;
    [SerializeField] private GameObject lightning;
    [SerializeField] private GameObject aarow;
    private float magicTimer = 0f;
    private float holdDuration; // Duration the action has been held down
    private float chargeTime = 0f;
    

    [Header("Dodge")]
    [SerializeField] private float dodgeCooldown = 3f;
    [SerializeField] private float dodgeDuration = .25f;
    [SerializeField] private float dodgeDistance = 2f;
    private float dodgeTimer = 0f;
    private bool canDodge = true;
    [SerializeField] DeactivateOnCollision DOC;

    public bool canMove=true;
    [SerializeField] private float spellDistance = 1f;

    void Awake()
    {
        playerInput = new PlayerInputActions();
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
      

        isWalkingHash = Animator.StringToHash("isWalking");
        isRunningHash = Animator.StringToHash("isRunning");
        DodgeHash = Animator.StringToHash("Dodge");
        
    }

    public void InitializePlayer(PlayerConfiguration config)
    {
        config.Input.onActionTriggered += Input_onActionTriggered;
    }

    private void Input_onActionTriggered(CallbackContext obj)
    {
        if (obj.action.name == playerInput.Player.Move.name)
        {
            onMovmentInput(obj);
        }
        //if (obj.action.name == playerInput.Player.Sprint.name)
        //{
        //    onRun(obj);
        //}
        if (obj.action.name == playerInput.Player.Fire.name)
        {
            onFire(obj);
        }
        if (obj.action.name == playerInput.Player.Ice.name)
        {
            onIce(obj);
        }
        if (obj.action.name == playerInput.Player.Earth.name)
        {
            onEarth(obj);
        }
        if(obj.action.name == playerInput.Player.Dodge.name)
        {
            onDodge(obj);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            handleRotation();
            handleAnimation();
            handleGravity();
            handleDodge();

            if (isRunPressed)
            {
                characterController.Move(currentRunMovement * Time.deltaTime);
            }
            else
            {
                characterController.Move(currentMovement*Time.deltaTime);
            }
        chargeTime += Time.deltaTime;
            UpdateCooldownTimers();
        }
        
        
    }

    void UpdateCooldownTimers()
    {
        magicTimer -= Time.deltaTime;
    }

    void handleAnimation()
    {
        bool isWalking = animator.GetBool(isWalkingHash);
        bool isRunning = animator.GetBool(isRunningHash);
        //bool Dodge = animator.GetBool(DodgeHash);


        if (isMovementPressed && !isWalking)
        {
            animator.SetBool(isWalkingHash, true);
        }else if(!isMovementPressed && isWalking)
        {
            animator.SetBool(isWalkingHash, false);
        }

        if ((isMovementPressed&& isRunPressed) && !isRunning)
        {
            animator.SetBool(isRunningHash, true);
        }else if ((!isMovementPressed || !isRunPressed) && isRunning)
        {
            animator.SetBool(isRunningHash, false);
        }
    }

    void handleGravity()
    {
        if (characterController.isGrounded)
        {
            float groundedGGravity = -.05f;
            currentMovement.y = groundedGGravity;
            currentRunMovement.y = groundedGGravity;
        }
        else
        {
            float gravity = -.8f;
            currentMovement.y += gravity;
            currentRunMovement.y += gravity;
        }
    }
    void onRun(InputAction.CallbackContext context)
        {
            isRunPressed = context.ReadValueAsButton();
        }

    void handleRotation()
    {
        Vector3 positionToLookAt;

        positionToLookAt.x = currentMovement.x;
        positionToLookAt.y = 0.0f;
        positionToLookAt.z = currentMovement.z;

        Quaternion currentRotation = transform.rotation;

        if (isMovementPressed)
        {
            Quaternion targetRotation = Quaternion.LookRotation(positionToLookAt);
            transform.rotation=Quaternion.Slerp(currentRotation, targetRotation, rotationFactorPerFrame*Time.deltaTime);
        }
    }

    void onFire(InputAction.CallbackContext context)
    {
        if (magicTimer <= 0f && !CheckForWallCollisionSpells())
        {
            if (context.phase == InputActionPhase.Started)
            {
                canDodge = false;
                isRunPressed = true;
                lightning.SetActive(true);
                aarow.SetActive(true);
                chargeTime = 0f;
            }

            else if (context.phase == InputActionPhase.Canceled)
            {
                // Calculate speed based on hold duration (up to a maximum)
                float speed = Mathf.Clamp(magicSpeed * chargeTime, magicSpeed / 3f, magicSpeed * 2f);
                lightning.SetActive(false);
                aarow.SetActive(false);
                // Instantiate magic with calculated speed
                InstantiateMagic(firePrefab, speed);
                magicTimer = magicCooldown;
                isRunPressed = false;
                canDodge = true;
            }
            
        }

    }
    void onIce(InputAction.CallbackContext context)
    {
        if (magicTimer <= 0f && !CheckForWallCollisionSpells())
        {
            if (context.phase == InputActionPhase.Started)
            {
                canDodge = false;
                isRunPressed = true;
                lightning.SetActive(true);
                aarow.SetActive(true);
                chargeTime = 0f;
            }

            else if (context.phase == InputActionPhase.Canceled)
            {
                // Calculate speed based on hold duration (up to a maximum)
                float speed = Mathf.Clamp(magicSpeed * chargeTime, magicSpeed / 3f, magicSpeed * 2f);
                lightning.SetActive(false);
                aarow.SetActive(false);
                // Instantiate magic with calculated speed
                InstantiateMagic(icePrefab, speed);
                magicTimer = magicCooldown;
                isRunPressed = false;
                canDodge = true;
            }

        }
    }
    void onEarth(InputAction.CallbackContext context)
    {
        if (magicTimer <= 0f && !CheckForWallCollisionSpells())
        {
            if (context.phase == InputActionPhase.Started)
            {
                canDodge = false;
                isRunPressed = true;
                lightning.SetActive(true);
                aarow.SetActive(true);
                chargeTime = 0f;
            }

            else if (context.phase == InputActionPhase.Canceled)
            {
                // Calculate speed based on hold duration (up to a maximum)
                float speed = Mathf.Clamp(magicSpeed * chargeTime, magicSpeed / 3f, magicSpeed * 2f);
                lightning.SetActive(false);
                aarow.SetActive(false);
                // Instantiate magic with calculated speed
                InstantiateMagic(earthPrefab, speed);
                magicTimer = magicCooldown;
                isRunPressed = false;
                canDodge = true;
            }

        }

    }

    private void OnEnable()
    {
        playerInput.Player.Enable();
    }
    private void OnDisable()
    {
        playerInput.Player.Disable();
    }
    void InstantiateMagic(GameObject magicPrefab, float speed)
    {
        if (magicTimer <= 0f)
        {
            GameObject magic = Instantiate(magicPrefab, firePosition.position, Quaternion.identity);
            if (magic != null)
            {
                magic.GetComponent<Rigidbody>().AddForce(transform.forward * speed);
            }
            magicTimer = magicCooldown;
        }
    }
    void onMovmentInput(InputAction.CallbackContext context)
    {
        currentMovementInput = context.ReadValue<Vector2>();
        currentMovement.x = currentMovementInput.x*3;
        currentMovement.z = currentMovementInput.y*3;
        currentRunMovement.x = currentMovement.x * runMultiplier;
        currentRunMovement.z = currentMovement.z * runMultiplier;
        isMovementPressed = currentMovementInput.x != 0 || currentMovementInput.y != 0;
    }

    void handleDodge()
    {
        dodgeTimer -= Time.deltaTime;

        if (dodgeTimer <= 0f)
        {
            canDodge = true;
        }
    }

    void onDodge(InputAction.CallbackContext context)
    {
        if (canDodge)
        {
            // Check if there is a wall in front of the player
            if (!CheckForWallCollision())
            {
                // Trigger the dodge animation
                animator.SetTrigger(DodgeHash);

                DOC.enabled = false;

                // Calculate the dodge direction based on the player's forward direction
                Vector3 dodgeDirection = transform.forward;

                // Calculate the target position for the dodge
                Vector3 targetPosition = transform.position + dodgeDirection * dodgeDistance;

                // Smoothly move the character towards the target position over the dodge duration
                StartCoroutine(MoveCharacterTowards(targetPosition, dodgeDuration));

                // Reset the dodge cooldown timer
                dodgeTimer = dodgeCooldown;

                // Set canDodge to false until the cooldown ends
                canDodge = false;
            }
        
        }
    }
    IEnumerator MoveCharacterTowards(Vector3 targetPosition, float duration)
    {
        float elapsedTime = 0f;
        Vector3 startingPosition = transform.position;

        while (elapsedTime < duration)
        {
            // Calculate the interpolation factor
            float t = elapsedTime / duration;

            // Lerp the position towards the target position
            transform.position = Vector3.Lerp(startingPosition, targetPosition, t);

            // Increment the elapsed time
            elapsedTime += Time.deltaTime;

            yield return null;
        }

        // Ensure the character reaches the exact target position
        transform.position = targetPosition;

        DOC.enabled = true;
    }
    
    bool CheckForWallCollision()
    {
        // Cast a ray forward to check for collisions with objects tagged as "Wall"
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, dodgeDistance))
        {
            if (hit.collider.CompareTag("wall"))
            {
                return true; // Wall collision detected
            }
        }
        return false; // No wall collision detected
    }
    bool CheckForWallCollisionSpells()
    {
        // Cast a ray forward to check for collisions with objects tagged as "Wall"
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, spellDistance))
        {
            if (hit.collider.CompareTag("wall"))
            {
                return true; // Wall collision detected
            }
        }
        return false; // No wall collision detected
    }
}
