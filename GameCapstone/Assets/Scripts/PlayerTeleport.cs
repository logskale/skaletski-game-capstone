using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTeleport : MonoBehaviour
{
    [SerializeField] private Transform[] levelOnePlayerSpawns;
    [SerializeField] private Transform[] levelTwoPlayerSpawns;
    [SerializeField] private Transform[] levelThreePlayerSpawns;

    public void TeleportPlayer(Transform playerTransform, Transform[] spawnPoints)
    {
        if (spawnPoints.Length > 0)
        {
            // Choose a random spawn point
            Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];

            // Teleport the player to the spawn point
            playerTransform.position = spawnPoint.position;
            //playerTransform.rotation = spawnPoint.rotation;
        }
        else
        {
            Debug.LogWarning("No spawn points found.");
        }
    }

}
