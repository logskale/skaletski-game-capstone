using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScore : MonoBehaviour
{
    // Variable to store the player's score
    public int score = 0;
    // Variable to store the name of the GameObject this script is attached to
    public string gameObjectName;

    void Start()
    {
        // Get the name of the GameObject this script is attached to
        gameObjectName = gameObject.name;
        Debug.Log("GameObject name: " + gameObjectName);
    }

    // Function to add points to the score
    public void AddPoints(int pointsToAdd)
    {
        score += pointsToAdd;
    }

    // Function to retrieve the current score
    public int GetScore()
    {
        return score;
    }

    // Function to reset the score to zero
    public void ResetScore()
    {
        score = 0;
    }
}
