using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
public abstract class CharacterBody : MonoBehaviour
{
    [Header("Character - Ground Movement")]
    [Tooltip("The current move speed of this character")]
    public float moveSpeed = 7f;

    [Header("Character - Air Movement")]
    [SerializeField] protected float jumpForce = 6f;

    [Header("Character - Rotation")]
    public float rotationSpeed = 10f;

    [Header("Character - Input")]
    protected Vector3 movementDirection; // The direction in which this character should move

    [Header("Character - Component/Object References")]
    [SerializeField] protected Animator animator;
    [SerializeField] protected CapsuleCollider capsuleCollider;
    [SerializeField] protected Rigidbody characterRigidbody;
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] protected Transform characterModel;

    #region Input

    public virtual void SetMoveInput(Vector2 moveInput)
    {
        throw new System.NotImplementedException();
    }

    #endregion

    #region Movement

    /// <summary>
    /// Called in every FixedUpdate to calculate movement.
    /// </summary>
    /// <exception cref="System.NotImplementedException"></exception>
    protected virtual void MoveCharacter()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Called in every Update to calculate rotation.
    /// </summary>
    /// <exception cref="System.NotImplementedException"></exception>
    protected virtual void RotateCharacter()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Tell the character to shoot.
    /// </summary>
    /// <exception cref="System.NotImplementedException"></exception>
    public virtual void Shoot()
    {
        throw new System.NotImplementedException();
    }

    /// <summary>
    /// Stop the jump early if it's still happening.
    /// </summary>
    /// <exception cref="System.NotImplementedException"></exception>
    public virtual void ShootCanceled()
    {
        throw new System.NotImplementedException();
    }

    #endregion
}
