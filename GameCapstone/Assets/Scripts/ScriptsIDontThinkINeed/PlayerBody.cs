using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

/// <summary>
/// This is the PlayerBody class, a specific child class of
/// the CharacterBody class. This script is meant to handle
/// special movement types for the player, and also handle
/// camera-related functionality.
/// </summary>

public class PlayerBody : CharacterBody
{
    #region Variables

    [Space(20)]

    [Header("Player - Ground Movement")]
    [SerializeField] private float moveAcceleration = 60f; // The speed at which this character accelerates in m/s
    [SerializeField] private float walkSpeed = 4f; // The max horizontal speed of this character (when walking) in m/s
    [SerializeField] private float sprintSpeed = 7f; // The max horizontal speed of this character (when running) in m/s
    private float maxVerticalMoveSpeed = 25f; // The maximum vertical move speed of this character
    private bool isSprinting = false; // Indicates whether you are sprinting or not
    Vector3 currentVelocity; // Used to store the current horizontal velocity of the rigidbody

    [Header("Player - Air Movement")]
    [SerializeField] private int maxJumps = 2; // The maximum number of times this character can jump before returning to the ground
    private int currentJump = 0; // The number of times you have jumped since leaving the ground
    [SerializeField] private float jumpCooldown = 0.25f; // The minimum amount of time that must elapse between jumps
    [SerializeField] private float airControlMultiplier = 0.4f; // The multiplier used to affect the amount of control you have in the air
    private bool readyToJump = true; // Boolean flag indicating if our jump cooldown is over

    [Header("Player - Rotation")]
    [SerializeField] private float playerGroundRotationSpeed = 10f; // The speed at which the player rotates (when grounded)
    [SerializeField] private float playerAirRotationSpeed = 3f; // The speed at which the player rotates (when airborne)

    [Header("Player - Ground Check")]
    [SerializeField] private float groundCheckDistance = 0.1f; // The distance below the player to check for ground
    [SerializeField] LayerMask environmentLayerMask; // Which layers are considered to be the environment
    private bool wasGroundedLastFrame; // Denotes whether you were on the ground last frame or not
    private bool isGrounded; // Denotes whether you are on the ground or not

    [Header("Player Input")]
    private Vector2 movementInput; // The Movement Input from our player

    [Header("Camera")]
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private Transform cameraOrientation;

    [Header("Magic")]
    [SerializeField] private GameObject firePrefab;
    [SerializeField] private GameObject earthPrefab;
    [SerializeField] private GameObject icePrefab;
    [SerializeField] private Transform firePosition;

    #endregion

    #region Unity Functions

    private void FixedUpdate()
    {
        // Move our character each frame.
        MoveCharacter();

        // Every physics update, make sure that
        // we are not exceeding our current
        // maximum allowed velocity.
        LimitVelocity();
    }

    private void Update()
    {
        // Rotate our character to face towards our input.
        RotateCharacter();
    }

    #endregion

    #region Custom Functions

    #region Input

    public override void SetMoveInput(Vector2 moveInput)
    {
        // Read in the Vector2 of our player input.
        movementInput = moveInput;

        movementDirection = new Vector3(movementInput.x, 0f, movementInput.y);
    }

    #endregion

    #region Movement

    protected override void MoveCharacter()
    {
        // We only need to apply forces to move
        // if we are trying to move. Thus, if we
        // aren't inputting anything, don't apply
        // any forces (they'd be 0 anyways).
        if (movementDirection != Vector3.zero)
        {
            // If we are on the ground we want to move according to our movespeed.
            if (isGrounded)
            {
                // Apply our movement Force.
                characterRigidbody.AddForce(movementDirection * moveAcceleration * characterRigidbody.mass, ForceMode.Force);
            }
            // Otherwise, if we are in the air we want to
            // move according to our movespeed modified by
            // our airControlMultiplier.
            else //Is in air
            {
                // Apply our movement force multiplied by
                // our airControlMultiplier.
                characterRigidbody.AddForce(movementDirection * moveAcceleration * airControlMultiplier * characterRigidbody.mass, ForceMode.Force);
            }
            
            Quaternion toRotation = Quaternion.LookRotation(movementDirection, Vector3.up);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, playerGroundRotationSpeed * Time.deltaTime);
        }
    }

    protected override void RotateCharacter()
    {

    }

    /// <summary>
    /// This function is called in every FixedUpdate call.
    /// This will ensure that if we are moving faster than
    /// our maximum allowed velocity, we will slow down to
    /// that maximum velocity.
    /// </summary>
    private void LimitVelocity()
    {
        // Limit Horizontal Velocity

        // We should constrain our velocity horizontally if we are on the ground.
        currentVelocity = GetHorizontalRBVelocity();

        // If our current velocity is greater than
        // our maximum allowed velocity
        float maxAllowedVelocity = GetMaxAllowedVelocity();
        if (currentVelocity.sqrMagnitude > (maxAllowedVelocity * maxAllowedVelocity))
        {
            // Use an impulse force to counteract our
            // velocity to slow down to max allowed velocity.
            Vector3 counteractDirection = currentVelocity.normalized * -1f;
            float counteractAmount = currentVelocity.magnitude - maxAllowedVelocity;
            characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
        }

        // Limit Vertical Velocity

        // If we are in the air, we should
        // constrain our vertical velocity.
        if (!isGrounded)
        {
            // If our current vertical velocity is
            // greater than our maximum vertical velocity.
            if (Mathf.Abs(characterRigidbody.velocity.y) > maxVerticalMoveSpeed)
            {
                // Use an impulse force to counteract our vertical
                // velocity to slow down to maxVerticalMoveSpeed.
                Vector3 counteractDirection = Vector3.up * Mathf.Sign(characterRigidbody.velocity.y) * -1f;
                float counteractAmount = Mathf.Abs(characterRigidbody.velocity.y) - maxVerticalMoveSpeed;
                characterRigidbody.AddForce(counteractDirection * counteractAmount * characterRigidbody.mass, ForceMode.Impulse);
            }
        }

    }

    /// <summary>
    /// This function is called when our shoot action
    /// is performed. It will launch magic
    /// in the direction the player is facing.
    /// </summary>
    public override void Shoot()
    {
        GameObject magic = Instantiate(firePrefab, firePosition.position, Quaternion.identity);
        magic.GetComponent<Rigidbody>().AddForce(transform.forward*1000);
    }

    /// <summary>
    /// This function is called jumpCooldown seconds after
    /// we jump. This function resets our ability to jump.
    /// </summary>
    private void HandleJumpCooldown()
    {

    }

    /// <summary>
    /// This function is called when our jump action
    /// is canceled. If we are still heading upwards,
    /// we will halve our vertical velocity. This enables
    /// us to perform partial jumps.
    /// </summary>
    public override void ShootCanceled()
    {

    }

    /// <summary>
    /// Tell the PlayerBody to begin sprinting.
    /// </summary>
    public void StartSprinting()
    {

    }

    /// <summary>
    /// Tell the PlayerBody to end sprinting.
    /// </summary>
    public void StopSprinting()
    {

    }

    #endregion

    #region Helper Functions

    /// <summary>
    /// This is a helper function that will strip the y component out of
    /// our Rigidbody velocity and give us back a vector with just the
    /// x and z (horizontal) components of our velocity.
    /// </summary>
    /// <returns>The XZ (horizontal) velocity Vector3.</returns>
    private Vector3 GetHorizontalRBVelocity()
    {
        return new Vector3(characterRigidbody.velocity.x, 0f, characterRigidbody.velocity.z);
    }

    /// <summary>
    /// This is a helper function that calculates the current
    /// maximum allowed movement velocity based on moveSpeed and
    /// the magnitude of our player movement input.
    /// </summary>
    /// <returns></returns>
    public float GetMaxAllowedVelocity()
    {
        return moveSpeed * movementDirection.magnitude;
    }

    #endregion

    #endregion
}
