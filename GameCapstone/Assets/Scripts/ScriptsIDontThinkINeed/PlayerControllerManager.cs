using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class PlayerControllerManager : MonoBehaviour
{
    private PlayerConfiguration playerConfig;
    private PlayerBody mover;

    [SerializeField] private MeshRenderer playerMesh;

    private PlayerInputActions controls;

    private void Awake()
    {
        mover = GetComponent<PlayerBody>();
        controls = new PlayerInputActions();
    }

    public void InitializePlayer(PlayerConfiguration config)
    {
        playerConfig = config;
        config.Input.onActionTriggered += Input_onActionTriggered;
    }
    private void Input_onActionTriggered(CallbackContext obj)
    {
        if (obj.action.name == controls.Player.Move.name)
        {
            OnMove(obj);
        }
    }

    public void OnMove(CallbackContext context)
    {
        if (mover != null)
        {
            
        }

    }

}
