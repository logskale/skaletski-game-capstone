using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerTagCheck : MonoBehaviour
{
    [SerializeField] string levelName;
    [SerializeField] Camera camera1;
    [SerializeField] Camera camera2;
    [SerializeField] Camera camera3;
    [SerializeField] Camera camera4;
    [SerializeField] Camera camera5;
    [SerializeField] Camera camera6;
    [SerializeField] float cameraPriority=10;
    [SerializeField] private Transform[] levelOnePlayerSpawns;
    [SerializeField] private Transform[] levelTwoPlayerSpawns;
    [SerializeField] private Transform[] levelThreePlayerSpawns;
    [SerializeField] private Transform[] levelFourPlayerSpawns;
    [SerializeField] private Transform[] levelFivePlayerSpawns;
    [SerializeField] private Transform[] levelSixPlayerSpawns;
    [SerializeField] private ActivateLeaderboard leaderboard;
    [SerializeField] private ActivateWinScreen winnerCanvas;
    [SerializeField] private PrintWinner winnerName;
    public int playNum = 0;
    public string playerName;

    public GameObject[] players;
    public int activePlayerCount;
    public int totalPlayers;
    private bool hasWon = false;

    private void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        totalPlayers = activePlayerCount;
        foreach (GameObject player in players)
        {
            player.GetComponent<AnimationAndMovementController>().canMove = true;


        }
    }

    void FixedUpdate()
    {
        CheckPlayerCount();

    }

    void CheckPlayerCount()
    {

        if (activePlayerCount <= 1)
        {
            foreach (GameObject player in players)
            {
                player.GetComponent<AnimationAndMovementController>().canMove=false;
                StartCoroutine(Wait());

            }
            
            PlayerScore playerScore = FindActivePlayerScore();
            if (playerScore != null)
            {
                // Give the player 100 points
                playerScore.AddPoints(100);
                //Win Screen
                if (playerScore.score >= 1000)
                {
                    PlayerScore highestScorePlayer = FindPlayerWithHighestScore();

                    if (highestScorePlayer != null)
                    {
                        // Assign playerName with the name of the GameObject with the highest score
                        playerName = highestScorePlayer.gameObjectName;
                    }
                    winnerName.winner = playerName;
                    //winnerName.winner=playNum;
                    winnerCanvas.ActivateCanvas();
                    hasWon = true;
                    StartCoroutine(DeactivateWinnerCanvasAfterDelay(4));
                    StartCoroutine(ResumeAfterDelay(4));
                    Time.timeScale = 0; // Pause the game
                    // Start a coroutine to resume the game after the specified duration
                    
                    GameObject playerConfigManager = GameObject.Find("PlayerConfigurationManager");
                    if (playerConfigManager != null)
                    {
                        Destroy(playerConfigManager);
                    }
                    
                }
            }

            DestroyGameObjectsWithTags("Wall", "Combination", "Fire", "Ice", "Earth");
            if (!hasWon)
            {
                leaderboard.ActivateCanvas();
                StartCoroutine(DeactivateCanvasAfterDelay(3f)); // Start coroutine to deactivate canvas after 10 seconds
            }
            foreach (GameObject player in players)
            {
                player.SetActive(true);

            }

            if (camera1 != null && camera2!=null && camera1.depth==cameraPriority)
            {
                //set camera for level 2 to be the new priority
                camera2.depth = cameraPriority;
                camera1.depth = 1;
                TeleportPlayersToSpawnPoints(players, levelTwoPlayerSpawns);
                //reset active player number
                activePlayerCount = totalPlayers;
            }
            else if (camera2 != null && camera3 != null && camera2.depth == cameraPriority)
            {
                //set camera for level 3 to be the new priority
                camera3.depth = cameraPriority;
                camera2.depth = 1;
                TeleportPlayersToSpawnPoints(players, levelThreePlayerSpawns);
                //reset active player number
                activePlayerCount = totalPlayers;
            }
            else if (camera3 != null && camera4 != null && camera3.depth == cameraPriority)
            {
                //set camera for level 4 to be the new priority
                camera4.depth = cameraPriority;
                camera3.depth = 1;
                TeleportPlayersToSpawnPoints(players, levelFourPlayerSpawns);
                //reset active player number
                activePlayerCount = totalPlayers;
            }
            else if (camera4 != null && camera5 != null && camera4.depth == cameraPriority)
            {
                //set camera for level 5 to be the new priority
                camera5.depth = cameraPriority;
                camera4.depth = 1;
                TeleportPlayersToSpawnPoints(players, levelFivePlayerSpawns);
                //reset active player number
                activePlayerCount = totalPlayers;
            }
            else if (camera5 != null && camera6 != null && camera5.depth == cameraPriority)
            {
                //set camera for level 5 to be the new priority
                camera6.depth = cameraPriority;
                camera5.depth = 1;
                TeleportPlayersToSpawnPoints(players, levelSixPlayerSpawns);
                //reset active player number
                activePlayerCount = totalPlayers;
            }
            else if (camera6 != null && camera1 != null && camera6.depth == cameraPriority)
            {
                //set camera for level 5 to be the new priority
                camera1.depth = cameraPriority;
                camera6.depth = 1;
                TeleportPlayersToSpawnPoints(players, levelOnePlayerSpawns);
                //reset active player number
                activePlayerCount = totalPlayers;
            }
        }
    }

    void DestroyGameObjectsWithTags(params string[] tags)
    {
        foreach (string tag in tags)
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(tag);

            foreach (GameObject gameObject in gameObjects)
            {
                Destroy(gameObject);
            }
        }
    }
    PlayerScore FindActivePlayerScore()
    {
        // Find all active GameObjects with the PlayerScore script attached
        PlayerScore[] playerScores = FindObjectsOfType<PlayerScore>();
        foreach (PlayerScore score in playerScores)
        {
            // Check if the GameObject is active
            if (score.gameObject.activeSelf)
            {
                playNum++;
                return score;
            }
            
        }
        playNum = 0;

        // If no active PlayerScore is found, return null
        return null;
    }

    PlayerScore FindPlayerWithHighestScore()
    {
        PlayerScore[] playerScores = FindObjectsOfType<PlayerScore>();
        PlayerScore highestScorePlayer = null;
        int highestScore = int.MinValue;

        foreach (PlayerScore score in playerScores)
        {
            if (score.score > highestScore)
            {
                highestScore = score.score;
                highestScorePlayer = score;
            }
        }

        return highestScorePlayer;
    }

    IEnumerator DeactivateCanvasAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay); // Wait for specified delay

        leaderboard.DeactivateCanvas(); // Deactivate canvas after delay
    }
    IEnumerator DeactivateWinnerCanvasAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay); // Wait for specified delay

        winnerCanvas.DeactivateCanvas(); // Deactivate canvas after delay
    }
    private IEnumerator ResumeAfterDelay(float duration)
    {
        yield return new WaitForSecondsRealtime(duration); // Wait for the specified duration
        Time.timeScale = 1; // Resume the game
        SceneManager.LoadScene("PlayerSelect");
    }

    void TeleportPlayersToSpawnPoints(GameObject[] players, Transform[] spawnPoints)
    {
        // Check if both players and spawnPoints arrays are valid
        if (players != null && spawnPoints != null && players.Length > 0 && spawnPoints.Length > 0)
        {
            // Ensure that the number of players does not exceed the number of spawn points
            int minCount = Mathf.Min(players.Length, spawnPoints.Length);

            // Teleport each player to their respective spawn point
            int i = 0;
            foreach (GameObject player in players)
            {
                // Ensure the index is within the range of spawnPoints
                if (i < minCount)
                {
                    player.transform.position = spawnPoints[i].position;
                    player.transform.rotation = spawnPoints[i].rotation;
                    i++;
                    Debug.Log("tp compleated");
                    Debug.Log(i+ "player teleoprted");
                }
                else
                {
                    Debug.LogWarning("Not enough spawn points for all players.");
                    break;
                }
            }
        }
        else
        {
            Debug.LogWarning("No players or spawn points to teleport.");
        }
    }
    IEnumerator Wait()
    {
        Debug.Log("Coroutine started");

        // Wait for 5 seconds
        yield return new WaitForSeconds(3);

        Debug.Log("5 seconds have passed");
        foreach (GameObject player in players)
        {
            player.GetComponent<AnimationAndMovementController>().canMove = true;


        }
        // Continue with the rest of the code here
    }
}