using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePlayersOnSceneLoad : MonoBehaviour
{
    void Start()
    {
        ActivateAllPlayers();
    }

    void ActivateAllPlayers()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject player in players)
        {
            player.SetActive(true);
        }
    }
}