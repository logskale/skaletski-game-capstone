using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class FireControls : MonoBehaviour
{
    [SerializeField] private GameObject firePrefab;
    private Transform firePosition;

    private void Start()
    {
    }

    void onFire(InputAction.CallbackContext context)
    {
        GameObject magic = Instantiate(firePrefab, firePosition.position, Quaternion.identity);
        magic.GetComponent<Rigidbody>().AddForce(transform.forward * 1000);
    }

}
