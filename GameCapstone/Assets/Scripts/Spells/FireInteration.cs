using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireInteration : MonoBehaviour
{
    [SerializeField] private GameObject fireFirePrefab;
    [SerializeField] private GameObject fireEarthPrefab;
    [SerializeField] private GameObject fireIcePrefab;

    private void OnCollisionEnter(Collision collision)
    {
        // Check if the colliding object has the specified magic tag
        if (collision.gameObject.CompareTag("Fire"))
        {
            // Get the positions of the two colliding spheres
            Vector3 position1 = transform.position;
            Vector3 position2 = collision.transform.position;

            // Calculate the midpoint between the two positions
            Vector3 midpoint = (position1 + position2) / 2f;

            // Instantiate a new game object at the midpoint
            GameObject newGameObject = Instantiate(fireFirePrefab, midpoint, Quaternion.identity);

            // Destroy the two colliding spheres
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }else if (collision.gameObject.CompareTag("Earth"))
        {
            // Get the positions of the two colliding spheres
            Vector3 position1 = transform.position;
            Vector3 position2 = collision.transform.position;

            // Calculate the midpoint between the two positions
            Vector3 midpoint = (position1 + position2) / 2f;

            // Instantiate a new game object at the midpoint
            GameObject newGameObject = Instantiate(fireEarthPrefab, midpoint, Quaternion.identity);

            // Destroy the two colliding spheres
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }else if (collision.gameObject.CompareTag("Ice"))
        {
            // Get the positions of the two colliding spheres
            Vector3 position1 = transform.position;
            Vector3 position2 = collision.transform.position;

            // Calculate the midpoint between the two positions
            Vector3 midpoint = (position1 + position2) / 2f;

            // Instantiate a new game object at the midpoint
            GameObject newGameObject = Instantiate(fireIcePrefab, midpoint, Quaternion.identity);

            // Destroy the two colliding spheres
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}