using UnityEngine;

public class DeactivateOnCollision : MonoBehaviour
{
    private PlayerTagCheck ptc;

    void Start()
    {
        // Find the PlayerTagCheck script in the scene
        ptc = FindObjectOfType<PlayerTagCheck>();
        if (ptc == null)
        {
            Debug.LogError("PlayerTagCheck not found in the scene!");
        }
    }

    // Called when a collision occurs
    private void OnCollisionEnter(Collision collision)
    {
        // Check if the collided object has one of the specified tags
        if (collision.gameObject.CompareTag("Fire") ||
            collision.gameObject.CompareTag("Ice") ||
            collision.gameObject.CompareTag("Earth") ||
            collision.gameObject.CompareTag("Combination") ||
            collision.gameObject.CompareTag("Death"))
        {
            // Deactivate the object this script is attached to
            gameObject.SetActive(false);
            if (ptc != null)
                ptc.activePlayerCount--;
        }
    }
    
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Death"))
        {
            // Deactivate the object this script is attached to
            gameObject.SetActive(false);
            if (ptc != null)
                ptc.activePlayerCount--;
        }
    }
}
