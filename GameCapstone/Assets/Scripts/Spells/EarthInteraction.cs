using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthInteraction : MonoBehaviour
{
    [SerializeField] private GameObject earthEarthPrefab;
    [SerializeField] private GameObject EarthIcePrefab;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Earth"))
        {
            // Get the positions of the two colliding spheres
            Vector3 position1 = transform.position;
            Vector3 position2 = collision.transform.position;

            // Calculate the midpoint between the two positions
            Vector3 midpoint = (position1 + position2) / 2f;

            // Instantiate a new game object at the midpoint
            GameObject newGameObject = Instantiate(earthEarthPrefab, midpoint, Quaternion.identity);

            // Destroy the two colliding spheres
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Ice"))
        {
            // Get the positions of the two colliding spheres
            Vector3 position1 = transform.position;
            Vector3 position2 = collision.transform.position;

            // Calculate the midpoint between the two positions
            Vector3 midpoint = (position1 + position2) / 2f;

            // Instantiate a new game object at the midpoint
            GameObject newGameObject = Instantiate(EarthIcePrefab, midpoint, Quaternion.identity);

            // Destroy the two colliding spheres
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
