using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceInteraction : MonoBehaviour
{

    [SerializeField] private GameObject iceIcePrefab;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ice"))
        {
            // Get the positions of the two colliding spheres
            Vector3 position1 = transform.position;
            Vector3 position2 = collision.transform.position;

            // Calculate the midpoint between the two positions
            Vector3 midpoint = (position1 + position2) / 2f;

            // Instantiate a new game object at the midpoint with a random direction
            GameObject iceSpike1 = Instantiate(iceIcePrefab, midpoint, Quaternion.identity);
            SetRandomDirection(iceSpike1);

            GameObject iceSpike2 = Instantiate(iceIcePrefab, midpoint, Quaternion.identity);
            SetRandomDirection(iceSpike2);

            // Destroy the two colliding spheres
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
    private void SetRandomDirection(GameObject iceSpike)
    {
        // Set a random direction for the ice spike
        Vector3 randomDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;
        iceSpike.GetComponent<Rigidbody>().AddForce(randomDirection * 1000);
    }
}
