using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteObject : MonoBehaviour
{
    [SerializeField] private float time = 4f;
    // Start is called before the first frame update
    void Start()
    {
        // Invoke the DestroyObject method after 4 seconds
        Invoke("DestroyObject", time);
    }

    // Method to destroy the game object
    void DestroyObject()
    {
        Destroy(gameObject);
    }
}
