using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class initilizeLevel : MonoBehaviour
{
    [SerializeField] private Transform[] playerSpawns;
    [SerializeField] private GameObject playerPrefab;
    private PlayerTagCheck ptc;


    void Awake()
    {
        // Find the PlayerTagCheck script in the scene
        ptc = FindObjectOfType<PlayerTagCheck>();
        if (ptc == null)
        {
            Debug.LogError("PlayerTagCheck not found in the scene!");
        }
        var playerConfigs = PlayerConfigurationManager.Instance.GetPlayerConfigs().ToArray();
        for (int i = 0; i < playerConfigs.Length; i++)
        {
            var player = Instantiate(playerPrefab, playerSpawns[i].position, playerSpawns[i].rotation, gameObject.transform);
            player.name = "Player" + (ptc.activePlayerCount + 1); // You may adjust this naming convention as needed
            player.GetComponent<AnimationAndMovementController>().InitializePlayer(playerConfigs[i]);
            ptc.activePlayerCount++;
        }

    }

}
