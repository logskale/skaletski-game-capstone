using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : MonoBehaviour
{
    [SerializeField] Material Red;
    [SerializeField] Material Blue;
    [SerializeField] Material Yellow;
    [SerializeField] Material Green;
    [SerializeField] SkinnedMeshRenderer SMR;

    PlayerConfigurationManager PCM;

    // Start is called before the first frame update
    void Start()
    {
        PCM = FindObjectOfType<PlayerConfigurationManager>();
        Debug.Log(PCM.colorChosen);

        if (PCM == null)
        {
            Debug.LogError("PlayerConfigurationManager not found in the scene.");
            return;
        }
        if (PCM.playerConfigs[0].PlayerMaterial.ToString() == "Red")
        {
            SMR.material = Red;
            Debug.Log("Changed color to red");
        }
        else if (PCM.playerConfigs[0].PlayerMaterial.ToString() == "Blue")
        {
            SMR.materials[3] = Blue;
            Debug.Log("Changed color to blue");
        }
        else if (PCM.playerConfigs[0].PlayerMaterial.ToString() == "Yellow")
        {
            SMR.material = Yellow;
            Debug.Log("Changed color to yellow");
        }
        else if (PCM.playerConfigs[0].PlayerMaterial.ToString() == "Green")
        {
            SMR.material = Green;
            Debug.Log("Changed color to green");
        }

        if (PCM.colorChosen.ToString() == "Red")
        {
            SMR.material = Red;

        }
        else if (PCM.colorChosen.ToString() == "Blue")
        {
            SMR.material = Blue;
        }
        else if (PCM.colorChosen.ToString() == "Yellow")
        {
            SMR.material = Yellow;
        }
        else if (PCM.colorChosen.ToString() == "Green")
        {
            SMR.material = Green;
        }
    }

}
