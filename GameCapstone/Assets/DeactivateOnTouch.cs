using UnityEngine;

public class DeactivateOnTouch : MonoBehaviour
{
    private PlayerTagCheck ptc;

    void Start()
    {
        ptc = FindObjectOfType<PlayerTagCheck>();
        if (ptc == null)
        {
            Debug.LogError("PlayerTagCheck not found in the scene!");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        // Check if the colliding object has the tag "Player"
        if (other.CompareTag("Player"))
        {
            // Deactivate this game object
            other.gameObject.SetActive(false);
            if (ptc != null)
                ptc.activePlayerCount--;
        }
    }
}
