using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerTwoScore : MonoBehaviour
{
    [SerializeField] PlayerTagCheck getPlayer;
    public TextMeshProUGUI textMeshPro;
    private bool isActive;

    // Start is called before the first frame update
    void Start()
    {
        if (getPlayer.players[1] != null)
        {
            if (textMeshPro == null)
                textMeshPro = GetComponent<TextMeshProUGUI>();
            isActive = true;
        }
        else
        {
            isActive = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            textMeshPro.text = "Player 2: " + getPlayer.players[1].GetComponent<PlayerScore>().GetScore();
        }
    }
}
