using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftTeleport : MonoBehaviour
{
    [SerializeField] private Transform teleportLocation; // Desired teleport location

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Ice") || other.CompareTag("Fire") || other.CompareTag("Earth") || other.CompareTag("Combination"))
        {
            TeleportObject(other.gameObject);
        }
    }

    private void TeleportObject(GameObject obj)
    {
        obj.transform.position = teleportLocation.position;

        if (obj.CompareTag("Fire") || obj.CompareTag("Ice") || obj.CompareTag("Earth"))
        {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddForce(Vector3.back * 20f, ForceMode.Impulse);
            }
            else
            {
                Debug.LogWarning("Rigidbody not found on object with tag: " + obj.tag);
            }
        }
    }
}