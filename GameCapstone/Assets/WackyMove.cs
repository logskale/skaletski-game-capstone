using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WackyMove : MonoBehaviour
{
    [SerializeField] float moveDistance = 2f;
    [SerializeField] float moveInterval = 0.25f;

    void Start()
    {
        // Start the movement coroutine
        StartCoroutine(MoveObject());
    }

    IEnumerator MoveObject()
    {
        while (true)
        {
            // Generate random direction in x and z axes
            Vector3 randomDirection = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized;

            // Calculate the target position for the movement
            Vector3 targetPosition = transform.position + randomDirection * moveDistance;

            // Smoothly move the object towards the target position
            float elapsedTime = 0f;
            Vector3 startPosition = transform.position;

            while (elapsedTime < moveInterval)
            {
                transform.position = Vector3.Lerp(startPosition, targetPosition, elapsedTime / moveInterval);
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            // Ensure the object reaches the exact target position
            transform.position = targetPosition;

            // Wait for the specified interval before the next movement
            yield return new WaitForSeconds(moveInterval);
        }
    }
}
