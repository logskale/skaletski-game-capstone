using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthComboMoveLeft : MonoBehaviour
{
    [SerializeField] float moveDistanceZ = 2f;
    [SerializeField] float moveDistanceX = 2f;
    [SerializeField] float moveInterval = 0.25f;
    [SerializeField] float startYPosition = -1.45f;
    [SerializeField] float endYPosition = 0.83f;

    void Start()
    {
        // Start the movement coroutine
        StartCoroutine(MoveObject());
    }

    IEnumerator MoveObject()
    {
        while (true)
        {
            // Smoothly move the object from startYPosition to endYPosition in the y-direction
            float elapsedTime = 0f;
            Vector3 startPosition = transform.position;
            Vector3 targetPosition = new Vector3(transform.position.x, endYPosition, transform.position.z);

            while (elapsedTime < moveInterval)
            {
                transform.position = Vector3.Lerp(startPosition, targetPosition, elapsedTime / moveInterval);
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            // Move the object by the specified distance in the +x direction
            transform.Translate(new Vector3(-moveDistanceX, 0, -moveDistanceZ));

            // Reset the y-position to the start height
            transform.position = new Vector3(transform.position.x, startYPosition, transform.position.z);

            // Wait for the specified interval before the next movement
            yield return new WaitForSeconds(moveInterval);
        }
    }
}