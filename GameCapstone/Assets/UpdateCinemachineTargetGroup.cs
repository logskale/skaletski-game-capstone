using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class UpdateCinemachineTargetGroup : MonoBehaviour
{
    private CinemachineTargetGroup targetGroup;

    void Start()
    {
        // Get the CinemachineTargetGroup component attached to the same GameObject
        targetGroup = GetComponent<CinemachineTargetGroup>();
        if (targetGroup == null)
        {
            Debug.LogError("CinemachineTargetGroup component not found!");
            return;
        }

        // Update the targets initially
        UpdateTargetGroup();
    }

    void LateUpdate()
    {
        // Update the targets each frame
        UpdateTargetGroup();
    }

    void UpdateTargetGroup()
    {
        // Find all active GameObjects with the tag "Player"
        GameObject[] playerObjects = GameObject.FindGameObjectsWithTag("Player");

        // Clear existing targets in the CinemachineTargetGroup
        targetGroup.m_Targets = new CinemachineTargetGroup.Target[0];

        // Create a list to hold new targets
        List<CinemachineTargetGroup.Target> newTargets = new List<CinemachineTargetGroup.Target>();

        // Add active player objects to the list of targets
        foreach (GameObject playerObject in playerObjects)
        {
            if (playerObject.activeInHierarchy)
            {
                Transform playerTransform = playerObject.transform;
                newTargets.Add(new CinemachineTargetGroup.Target
                {
                    target = playerTransform,
                    weight = 1f,
                    radius = 0f // Set the radius as needed
                });
            }
        }

        // Assign the new list of targets to the CinemachineTargetGroup
        targetGroup.m_Targets = newTargets.ToArray();
    }
}
