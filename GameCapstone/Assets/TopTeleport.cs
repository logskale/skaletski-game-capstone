using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopTeleport : MonoBehaviour
{
    [SerializeField] private Transform teleportLocation; // Desired teleport location

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Ice") || other.CompareTag("Fire") || other.CompareTag("Earth") || other.CompareTag("Combination"))
        {
            StartCoroutine(TeleportObjectCoroutine(other.gameObject));
        }
    }

    private IEnumerator TeleportObjectCoroutine(GameObject obj)
    {
        // Wait until the end of the frame to ensure all physics interactions are processed
        yield return new WaitForEndOfFrame();

        obj.transform.position = teleportLocation.position;

        if (obj.CompareTag("Fire") || obj.CompareTag("Ice") || obj.CompareTag("Earth"))
        {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddForce(Vector3.right * 20f, ForceMode.Impulse);
            }
            else
            {
                Debug.LogWarning("Rigidbody not found on object with tag: " + obj.tag);
            }
        }
    }
}