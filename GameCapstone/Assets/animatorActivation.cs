using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animatorActivation : MonoBehaviour
{
    private bool hasActivated = false;
    private Animator animator;

    void Start()
    {
        // Get the Animator component attached to the GameObject
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // Check if the animator has not been activated yet and the condition to activate it is met
        if (!hasActivated)
        {
            this.animator.Rebind();
            this.animator.Update(0f);
            // Set the flag to true to indicate that the animator has been activated
            hasActivated = true;
            
        }
    }

    
}
