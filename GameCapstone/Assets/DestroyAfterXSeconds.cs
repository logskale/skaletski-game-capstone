using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterXSeconds : MonoBehaviour
{
    [SerializeField] float Seconds = 4f;
    void Start()
    {
        // Invoke the DestroyObject method after 4 seconds
        Invoke("DestroyObject", Seconds);
    }

    void DestroyObject()
    {
        // Destroy the GameObject this script is attached to
        Destroy(gameObject);
    }
}
