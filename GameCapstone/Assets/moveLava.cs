using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveLava : MonoBehaviour
{

    [SerializeField] float moveSpeed = 2f;

    void Start()
    {
        // Start the movement coroutine
        StartCoroutine(MoveObject());
    }

    IEnumerator MoveObject()
    {
        while (true)
        {
            // Generate random direction in x and z axes
            Vector3 moveDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;

            // Move the object continuously in the randomly generated direction
            while (true)
            {
                transform.Translate(moveDirection * moveSpeed * Time.deltaTime);
                yield return null;
            }
        }
    }
}
