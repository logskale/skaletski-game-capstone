using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerThreeScore : MonoBehaviour
{
    [SerializeField] PlayerTagCheck getPlayer;
    public TextMeshProUGUI textMeshPro;
    private bool isActive;

    // Start is called before the first frame update
    void Start()
    {
        if (getPlayer.totalPlayers > 2)
        {
            if (getPlayer.players[2] != null)
            {
                if (textMeshPro == null)
                    textMeshPro = GetComponent<TextMeshProUGUI>();
                isActive = true;
            }
            else
            {
                isActive = false;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            textMeshPro.text = "Player 3: " + getPlayer.players[2].GetComponent<PlayerScore>().GetScore();
        }
    }
}
