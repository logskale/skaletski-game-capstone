using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceburgSinking : MonoBehaviour
{
    [SerializeField] private GameObject iceberg1;
    [SerializeField] private GameObject iceberg2;
    [SerializeField] private GameObject iceberg3;
    [SerializeField] private GameObject iceberg4;
    [SerializeField] private Camera targetCamera;
    [SerializeField] private float shakeIntensity = 0.1f;
    [SerializeField] private float shakeDuration = 3f;

    private GameObject[] icebergs;
    private float originalY;
    private float timer = 0f;
    private bool movingDown = false;

    void Start()
    {
        // Store all icebergs in an array
        icebergs = new GameObject[] { iceberg1, iceberg2, iceberg3, iceberg4 };

        // Get original Y position of all icebergs
        originalY = iceberg1.transform.position.y;
    }

    void Update()
    {
        // Check if the target camera has a priority of 10
        if (targetCamera != null && targetCamera.depth == 10)
        {
            timer += Time.deltaTime;

            // Check if it's time to move the iceberg
            if (timer >= 10f)
            {
                // Reset timer
                timer = 0f;

                // Select a random iceberg
                GameObject randomIceberg = icebergs[Random.Range(0, icebergs.Length)];

                // Move the iceberg down
                if (!movingDown)
                {
                    StartCoroutine(ShakeIceberg(randomIceberg.transform, shakeIntensity, shakeDuration)); // Shake the iceberg
                    StartCoroutine(MoveIceberg(randomIceberg.transform, new Vector3(randomIceberg.transform.position.x, -100f, randomIceberg.transform.position.z), 1f));
                    movingDown = true;
                }
                // Move the iceberg back to its original position
                else
                {
                    StartCoroutine(MoveIceberg(randomIceberg.transform, new Vector3(randomIceberg.transform.position.x, originalY, randomIceberg.transform.position.z), 1f));
                    movingDown = false;
                }
            }
        }
        else
        {
            // If the camera doesn't have a priority of 10, keep the icebergs at their original positions
            foreach (GameObject iceberg in icebergs)
            {
                iceberg.transform.position = new Vector3(iceberg.transform.position.x, originalY, iceberg.transform.position.z);
            }
            timer = 0f; // Reset the timer
            movingDown = false; // Reset the moving down flag
        }
    }

    IEnumerator MoveIceberg(Transform icebergTransform, Vector3 targetPosition, float duration)
    {
        yield return new WaitForSeconds(2f);
        float time = 0f;
        Vector3 startPosition = icebergTransform.position;

        while (time < duration)
        {
            icebergTransform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return null;
        }

        icebergTransform.position = targetPosition;
    }

    IEnumerator ShakeIceberg(Transform icebergTransform, float intensity, float duration)
    {
        Vector3 originalPosition = icebergTransform.position;
        float endTime = Time.time + duration;

        while (Time.time < endTime)
        {
            icebergTransform.position = originalPosition + Random.insideUnitSphere * intensity;
            yield return null;
        }

        icebergTransform.position = originalPosition;
    }
}