using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateLeaderboard : MonoBehaviour
{
    [SerializeField] Canvas leaderBoardCanvas;

    public void ActivateCanvas()
    {
        leaderBoardCanvas.enabled=true;
    }
    public void DeactivateCanvas()
    {
        leaderBoardCanvas.enabled = false;
    }
}
