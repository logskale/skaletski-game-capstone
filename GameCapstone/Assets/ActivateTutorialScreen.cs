using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateTutorialScreen : MonoBehaviour
{
    [SerializeField] Canvas TutorialCanvas;

    public void ActivateCanvas()
    {
        TutorialCanvas.enabled = true;
    }
    public void DeactivateCanvas()
    {
        TutorialCanvas.enabled = false;
    }
}
