using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CouldronExplode : MonoBehaviour
{
    [SerializeField] GameObject explosionPrefab;

    void OnTriggerEnter(Collider other)
    {
        // Check if the collided object is tagged as "spell" or "combination"
        if (other.CompareTag("Fire") || other.CompareTag("combination") || other.CompareTag("Ice") || other.CompareTag("Earth"))
        {
            // Destroy the collided object
            Destroy(other.gameObject);

            // Spawn the explosion prefab at the location of this object
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        }
    }
}
